// test for creation of new board
var testChessBoard;
var testBoolBoard;

// test for white ColorSquares function
QUnit.test( "Get white squares", function( assert ) {
  testChessBoard = new ChessBoard;

  testBoolBoard = falseArray();
  testBoolBoard[0] = [true, true, true, true, true, true, true, true];
  testBoolBoard[1] = [true, true, true, true, true, true, true, true];

  assert.ok(arraysEqual(testChessBoard.GetColorSquares(ColorEnum.WHITE), testBoolBoard), "Passed!" );
});

// test for black ColorSquares function
QUnit.test( "Get black squares", function( assert ) {
  testChessBoard = new ChessBoard;

  testBoolBoard = falseArray();
  testBoolBoard[6] = [true, true, true, true, true, true, true, true];
  testBoolBoard[7] = [true, true, true, true, true, true, true, true];

  assert.ok(arraysEqual(testChessBoard.GetColorSquares(ColorEnum.BLACK), testBoolBoard), "Passed!" );
});

// test for starting pawn moves
QUnit.test( "Starting pawn moves", function( assert ) {
  testChessBoard = new ChessBoard;

  testBoolBoard = falseArray();
  testBoolBoard[2][0] = true;
  testBoolBoard[3][0] = true;

  assert.ok(arraysEqual(testChessBoard.GetValidMoves(0,1), testBoolBoard), "Passed!" );
});

// test for non-starting pawn moves and captures
QUnit.test( "Non-starting pawn moves and diagonal capture", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[2][0] = {piece: PieceEnum.PAWN, color: ColorEnum.WHITE};
  testChessBoard.board[3][1] = {piece: PieceEnum.PAWN, color: ColorEnum.BLACK};

  testBoolBoard = falseArray();
  testBoolBoard[3][0] = true; // forward move
  testBoolBoard[3][1] = true; // capture

  assert.ok(arraysEqual(testChessBoard.GetValidMoves(0,2), testBoolBoard), "Passed!" );
});

// test for rook moves
QUnit.test( "Rook moves", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.ROOK, color: ColorEnum.WHITE};
  testChessBoard.board[6][4] = {piece: PieceEnum.PAWN, color: ColorEnum.BLACK}; // capture should be valid, but should block moves through it

  testBoolBoard = falseArray();
  for (var i = 0; i <= 6; i++) { // vertical moves
    testBoolBoard[i][4] = true;
  }
  for (var i = 0; i < 8; i++) { // horizontal moves
    testBoolBoard[4][i] = true;
  }
  testBoolBoard[4][4] = false; // don't allow piece to move to its own square

  assert.ok(arraysEqual(testChessBoard.GetValidMoves(4,4), testBoolBoard), "Passed!" );
});

// test for bishop moves
QUnit.test( "Bishop moves", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.BISHOP, color: ColorEnum.WHITE};
  testChessBoard.board[6][6] = {piece: PieceEnum.PAWN, color: ColorEnum.BLACK}; // capture should be valid, but should block moves through it

  testBoolBoard = [[true , false, false, false, false, false, false, false],
               [false, true , false, false, false, false, false, true ],
               [false, false, true , false, false, false, true , false],
               [false, false, false, true , false, true , false, false],
               [false, false, false, false, false, false, false, false],
               [false, false, false, true , false, true , false, false],
               [false, false, true , false, false, false, true , false],
               [false, true , false, false, false, false, false, false]];

  assert.ok(arraysEqual(testChessBoard.GetValidMoves(4,4), testBoolBoard), "Passed!" );
});

// test for knight moves
QUnit.test( "Knight moves", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.KNIGHT, color: ColorEnum.WHITE};

  testBoolBoard = [[false, false, false, false, false, false, false, false],
               [false, false, false, false, false, false, false, false],
               [false, false, false, true , false, true , false, false],
               [false, false, true , false, false, false, true , false],
               [false, false, false, false, false, false, false, false],
               [false, false, true , false, false, false, true , false],
               [false, false, false, true , false, true , false, false],
               [false, false, false, false, false, false, false, false]];

  assert.ok(arraysEqual(testChessBoard.GetValidMoves(4,4), testBoolBoard), "Passed!" );
});

// test for queen moves
QUnit.test( "Queen moves", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.QUEEN, color: ColorEnum.WHITE};
  testChessBoard.board[6][6] = {piece: PieceEnum.PAWN, color: ColorEnum.BLACK}; // capture should be valid, but should block moves through it

  testBoolBoard = [[true , false, false, false, true , false, false, false],
               [false, true , false, false, true , false, false, true ],
               [false, false, true , false, true , false, true , false],
               [false, false, false, true , true , true , false, false],
               [true , true , true , true , false, true , true , true ],
               [false, false, false, true , true , true , false, false],
               [false, false, true , false, true , false, true , false],
               [false, true , false, false, true , false, false, false]];

  assert.ok(arraysEqual(testChessBoard.GetValidMoves(4,4), testBoolBoard), "Passed!" );
});

// test for king moves
QUnit.test( "King moves", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};

  testBoolBoard = [[false, false, false, false, false, false, false, false],
               [false, false, false, false, false, false, false, false],
               [false, false, false, false, false, false, false, false],
               [false, false, false, true , true , true , false, false],
               [false, false, false, true , false, true , false, false],
               [false, false, false, true , true , true , false, false],
               [false, false, false, false, false, false, false, false],
               [false, false, false, false, false, false, false, false]];

  assert.ok(arraysEqual(testChessBoard.GetValidMoves(4,4), testBoolBoard), "Passed!" );
});

// test that IsInCheck returns false when the player is not in check
QUnit.test( "IsInCheck false", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};
  testChessBoard.board[6][6] = {piece: PieceEnum.ROOK, color: ColorEnum.BLACK}; // cannot attack the king

  assert.ok(!testChessBoard.IsInCheck(ColorEnum.WHITE), "Passed!" );
});

// test that IsInCheck returns false when the player is not in check
QUnit.test( "IsInCheck true", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};
  testChessBoard.board[6][6] = {piece: PieceEnum.BISHOP, color: ColorEnum.BLACK}; // can attack the king

  assert.ok(testChessBoard.IsInCheck(ColorEnum.WHITE), "Passed!" );
});

// test that Move changes the correct squares
QUnit.test( "Valid move", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};
  var result = testChessBoard.Move(4,4,4,3); // check return value

  assert.ok(testChessBoard.board[4][4].piece == PieceEnum.EMPTY // origin square is now empty
    && testChessBoard.board[3][4].piece == PieceEnum.KING // target square is a king
    && testChessBoard.board[3][4].color == ColorEnum.WHITE // and is white
    && result, "Passed!" ); // function returns true
});

// test that Move rejects a move which is invalid by GetValidMoves
QUnit.test( "Invalid move", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};
  var result = testChessBoard.Move(4,4,4,2); // check return value

  assert.ok(testChessBoard.board[4][4].piece == PieceEnum.KING // origin square is still a king
    && testChessBoard.board[4][4].color == ColorEnum.WHITE // and is white
    && testChessBoard.board[3][4].piece == PieceEnum.EMPTY // target square is still empty
    && !result, "Passed!" ); // function returns faLse
});

// test that Move rejects a move which is valid by GetValidMoves but results in check
QUnit.test( "Move resulting in check", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[4][4] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};
  testChessBoard.board[6][6] = {piece: PieceEnum.KNIGHT, color: ColorEnum.BLACK}; // places [4,5] and [5,4] in check, so they chould not be valid moves
  var result = testChessBoard.Move(4,4,4,5); // check return value

  assert.ok(testChessBoard.board[4][4].piece == PieceEnum.KING // origin square is still a king
    && testChessBoard.board[4][4].color == ColorEnum.WHITE // and is white
    && testChessBoard.board[5][4].piece == PieceEnum.EMPTY // target square is still empty
    && !result, "Passed!" ); // function returns faLse
});

// test that CanMove returns true when a player can move, and the board is not changed
QUnit.test( "CanMove with valid movement", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[0][0] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};
  testChessBoard.board[1][7] = {piece: PieceEnum.ROOK, color: ColorEnum.BLACK}; // one rook is not enough to stop the king from moving
  var result = testChessBoard.CanMove(ColorEnum.WHITE); // check return value

  assert.ok(testChessBoard.board[0][0].piece == PieceEnum.KING // origin square is still a king
    && testChessBoard.board[0][0].color == ColorEnum.WHITE // and is white
    && testChessBoard.board[0][1].piece == PieceEnum.EMPTY // all tested moves are still empty
    && testChessBoard.board[1][1].piece == PieceEnum.EMPTY
    && testChessBoard.board[1][0].piece == PieceEnum.EMPTY
    && result, "Passed!" ); // function returns true
});

// test that CanMove returns false when a player cannot move, and the board is not changed
QUnit.test( "CanMove without valid movement", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[0][0] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};
  testChessBoard.board[1][7] = {piece: PieceEnum.ROOK, color: ColorEnum.BLACK}; // two rooks stop the king from moveing, but do not put it in check
  testChessBoard.board[7][1] = {piece: PieceEnum.ROOK, color: ColorEnum.BLACK};
  var result = testChessBoard.CanMove(ColorEnum.WHITE); // check return value

  assert.ok(testChessBoard.board[0][0].piece == PieceEnum.KING // origin square is still a king
    && testChessBoard.board[0][0].color == ColorEnum.WHITE // and is white
    && testChessBoard.board[0][1].piece == PieceEnum.EMPTY // all tested moves are still empty
    && testChessBoard.board[1][1].piece == PieceEnum.EMPTY
    && testChessBoard.board[1][0].piece == PieceEnum.EMPTY
    && !result, "Passed!" ); // function returns true
});

// test that IsInCheckmate returns false if the player is not in checkmate
QUnit.test( "IsInCheckmate false", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[0][0] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};
  testChessBoard.board[1][7] = {piece: PieceEnum.ROOK, color: ColorEnum.BLACK}; // two rooks stop the king from moveing, but do not put it in check
  testChessBoard.board[7][1] = {piece: PieceEnum.ROOK, color: ColorEnum.BLACK};

  assert.ok(!testChessBoard.IsInCheckmate(ColorEnum.WHITE), "Passed!" ); // function returns true
});

// test that IsInCheckmate returns true if the player is in checkmate
QUnit.test( "IsInCheckmate true", function( assert ) {
  testChessBoard = new ChessBoard;
  emptyChessBoard();
  testChessBoard.board[0][0] = {piece: PieceEnum.KING, color: ColorEnum.WHITE};
  testChessBoard.board[1][7] = {piece: PieceEnum.ROOK, color: ColorEnum.BLACK}; // two rooks stop the king from moveing
  testChessBoard.board[7][1] = {piece: PieceEnum.ROOK, color: ColorEnum.BLACK};
  testChessBoard.board[7][7] = {piece: PieceEnum.BISHOP, color: ColorEnum.BLACK}; // bishop puts the king under attack

  console.log('1');
  console.log(testChessBoard.IsInCheck(ColorEnum.WHITE));
  assert.ok(testChessBoard.IsInCheckmate(ColorEnum.WHITE), "Passed!" ); // function returns true
});

// helper functions for testing
function emptyChessBoard() {
  for (var i = 0; i < 8; i++) {
    for (var j = 0; j < 8; j++) {
      testChessBoard.board[i][j].piece = PieceEnum.EMPTY;
    }
  }
}

function falseArray() {
  var array = new Array(8);
  for (var i = 0; i < 8; i++) {
    array[i] = new Array(8);
    for (var j = 0; j < 8; j++) {
      array[i][j] = false;
    }
  }
  return array;
}

function arraysEqual(a, b) {
  //if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;

  for (var i = 0; i < a.length; i++) {
    if (Array.isArray(a[i]) && Array.isArray(b[i])) {
      if (!arraysEqual(a[i],b[i])) return false;
    }
    else {
      if (a[i] != b[i]) return false;
    }
  }
  return true;
}
