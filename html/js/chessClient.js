var socket;
var clientID = null;
var timer = null;
var timerSeconds = 0;
var turnSeconds = 120;

StateEnum = {
  DISCONNECTED : 1,
  CONNECTED : 2,
  READY : 3,
  MOVING : 4,
  WAITING : 5
}

var chessBoard = new ChessBoard();
var playerColor = ColorEnum.WHITE;
var state = StateEnum.DISCONNECTED;
var selectedX = -1;
var selectedY = -1;

function connect() {
  if (clientID == null) {
    console.log('creating socket');
    //socket = new WebSocket('ws://35.177.156.75:1337');
    socket = new WebSocket('ws://' + (window.location.hostname || 'localhost') + ':1337');
    console.log(socket);

    socket.onopen = function() {
      //socket.send('hello from the client');
    };

    socket.onmessage = function(evt) {
      var msg = JSON.parse(evt.data);
      console.log(msg.data);
      console.log(msg.type);

      switch (msg.type) {
        case 'create': // create a new connection
          chessBoard = new ChessBoard()
          clientID = msg.id;
          console.log('opened new connection ID: ' + msg.id);
          $('#instruction').text('Waiting for opponent to join...');
          $('#join').hide();
          break;
        case 'color':
          playerColor = msg.color;
          generateBoard(msg.color);
          console.log('joined game as ' + ((playerColor == ColorEnum.WHITE) ? 'white' : 'black'));
          break;
        case 'startGame':
          startGame();
          setTurn(ColorEnum.WHITE);
          break;
        case 'move':
          chessBoard.Move(msg.originX, msg.originY, msg.targetX, msg.targetY);
          setTurn(msg.turn);
          break;
        case 'endGame':
          endGame(msg.winner);
          stopTimer();
          break;
        default:
          console.log('unknown message type: ' + msg.type);
          break;
      }
    }

    socket.onclose = function() {
      clientID = null;
      console.log('closed');
    }
  }
  else {
    console.log('already connected');
  }
}

//==============================================================================
// fundtions to send messages to the server
//==============================================================================
function movePiece(ox,oy,tx,ty) {
  var msg = {
    type: "move",
    originX: ox,
    originY: oy,
    targetX: tx,
    targetY: ty,
  }
  socket.send(JSON.stringify(msg));
}

function resign() {
  var msg = {
    type: "resign",
  }
  socket.send(JSON.stringify(msg));
}

//==============================================================================
// timer functions
//==============================================================================
function setTimer() {
  stopTimer();
  timerSeconds = turnSeconds;
  timer = setInterval(function(){tickTimer()}, 1000);
  $('#timer').text('Timeout in ' + timerString(timerSeconds));
}

function tickTimer() {
  timerSeconds--;
  $('#timer').text('Timeout in ' + timerString(timerSeconds));
}

function stopTimer() {
  if (timer) {clearInterval(timer)};
}

function timerString(seconds) {
  return (Math.floor(seconds / 60) + ':' + ('0' + (seconds % 60)).slice(-2));
}

//==============================================================================
// on page load
//==============================================================================

generateBoard(playerColor);
$('#newGame').click(function() {
  connect();
});
$('#resign').click(function() {
  resign();
});
endGame();
console.log("Initialized");

//==============================================================================
// functions set UI to game mode or waiting mode
//==============================================================================
function startGame() {
  //$('#instruction').show();
  $('#timer').show();
  $('#timer').text('');
  $('#check').show();
  $('#check').text('');
  $('#controls').show();
  $('#join').hide();
}

function endGame(winner) {
  if (winner == null) {
    $('#instruction').text('Welcome to Chess');
  }
  else {
    if (winner == playerColor) {
      $('#instruction').text('You win');
    }
    else {
      $('#instruction').text('You lose');
    }
  }
  $('#timer').hide();
  $('#check').hide();
  $('#controls').hide();
  $('#join').show();
}

//==============================================================================
// generates the displayed board
//==============================================================================
function generateBoard(color) {
  // check for color so that square IDs are assigned in the right place relative to the player's view
  if (color == ColorEnum.WHITE) {
    $('#board').empty();

    for (var i = 0; i < 8; i++) {
      $('#board').prepend('<tr id="' + i + '"></tr>');
      for (var j = 0; j < 8; j++) {
        $('#' + i).append('<td id="' + j + '' + i + (((i + j) % 2 == 0) ? '" class="blacksq' : '') + '">?</td>'); // use ? to identify if pieces are not displayed properly
        // bind click event to the square
        $('#' + j + '' + i).click(function() {
          selectPiece(this.id);
        });
      }
    }

    console.log('new white board');
  }

  else if (ColorEnum.BLACK) {
    $('#board').empty();

    for (var i = 0; i < 8; i++) {
      $('#board').append('<tr id="' + i + '"></tr>');
      for (var j = 0; j < 8; j++) {
        $('#' + i).prepend('<td id="' + j + '' + i + (((i + j) % 2 == 0) ? '" class="blacksq' : '') + '">?</td>'); // use ? to identify if pieces are not displayed properly
        // bind click event to the square
        $('#' + j + '' + i).click(function() {
          selectPiece(this.id);
        });
      }
    }

    console.log('new black board');
  }

  displayPieces(chessBoard);
}

function displayPieces(board) {
  for (var i = 0; i < 8; i++) {
    for (var j = 0; j < 8; j++) {
      var square = board.board[i][j];
      $('#' + j + '' + i).removeClass('white black');
      $('#' + j + '' + i).addClass((square.color == ColorEnum.WHITE) ? 'white' : 'black');
      $('#' + j + '' + i).text(PieceIcon(square.piece));
    }
  }
}

function highlightSquare(x,y) {
  for (var i = 0; i < 8; i++) {
    for (var j = 0; j < 8; j++) {
      if (i == y && j == x) {
        $('#' + j + '' + i).addClass('singleHighlight');
      }
      else {
        $('#' + j + '' + i).removeClass('singleHighlight');
      }
    }
  }
}

function highlightSquares(board) {
  for (var i = 0; i < 8; i++) {
    for (var j = 0; j < 8; j++) {
      if (board[i][j]) {
        $('#' + j + '' + i).addClass('multiHighlight');
      }
      else {
        $('#' + j + '' + i).removeClass('multiHighlight');
      }
    }
  }
}

function unhighlightAllSquares() {
  for (var i = 0; i < 8; i++) {
    for (var j = 0; j < 8; j++) {
      $('#' + j + '' + i).removeClass('singleHighlight');
      $('#' + j + '' + i).removeClass('multiHighlight');
    }
  }
}

//==============================================================================
// updates the UI for a particular player's turn
//==============================================================================
function setTurn(color) {
  if (playerColor == color) {
    state = StateEnum.READY;
    $('#instruction').text('Click a yellow square to move the piece, or any other square to cancel');
  }
  else {
    state = StateEnum.WAITING;
    $('#instruction').text('Waiting for opponent to move...');
  }

  if (chessBoard.IsInCheck(playerColor)) {
    $('#check').text('You are in check!');
  }
  else {
    $('#check').text('');
  }

  setTimer();

  displayPieces(chessBoard);
}

//==============================================================================
// handles clicking on squares on the board
//==============================================================================
function selectPiece(xy) {
  var x = xy[0] * 1;
  var y = xy[1] * 1;

  // console.log(x);
  // console.log(y);

  switch (state) {
    case StateEnum.READY:
      if (chessBoard.GetColorSquares(playerColor)[y][x]) {
        //console.log('valid');

        //console.log(x + ' ' + y);
        highlightSquare(x,y);
        //console.log(chessBoard.GetValidMoves(x,y,false));
        highlightSquares(chessBoard.GetValidMoves(x,y,false));
        selectedX = x;
        selectedY = y;
        state = StateEnum.MOVING;
        $('#instruction').text('Click a yellow square to move the piece, or any other square to cancel');
      }
      else {
        //console.log('invalid');
      }
      break;
    case StateEnum.MOVING:
      if (chessBoard.GetValidMoves(selectedX,selectedY,false)[y][x]) {
        // send move to server

        if (chessBoard.Move(selectedX,selectedY,x,y)) {
          movePiece(selectedX,selectedY,x,y);
          displayPieces(chessBoard);
          unhighlightAllSquares();

          // state = StateEnum.READY;
          // displayPieces(chessBoard);
          // unhighlightAllSquares();
          // $('#instruction').text('Waiting for opponent to move');
        }
        else {
          $('#instruction').text('You cannot move here as you will be in check');
        }
      }
      else {
        // cancel the move
        console.log('cancelled the move');
        state = StateEnum.READY;
        unhighlightAllSquares();
        $('#instruction').text('Click a piece to move it');
      }
      break;
    default:
      break;
  }

  //$('#instruction').text(state);
}
