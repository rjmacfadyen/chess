PieceEnum = {
  EMPTY : 1,
  PAWN : 2,
  BISHOP : 3,
  KNIGHT : 4,
  ROOK : 5,
  QUEEN : 6,
  KING : 7
}

ColorEnum = {
  WHITE : 1,
  BLACK : 2
}

function PieceCode(p) {
  switch (p) {
    case PieceEnum.PAWN:
      return 'p';
      break;
    case PieceEnum.BISHOP:
      return 'B';
      break;
    case PieceEnum.KNIGHT:
      return 'N';
      break;
    case PieceEnum.ROOK:
      return 'R';
      break;
    case PieceEnum.QUEEN:
      return 'Q';
      break;
    case PieceEnum.KING:
      return 'K';
      break;
    default:
      return '';
      break;
  }
}

function PieceIcon(p) {
  switch (p) {
    case PieceEnum.PAWN:
      return '\u265f';
      break;
    case PieceEnum.BISHOP:
      return '\u265d';
      break;
    case PieceEnum.KNIGHT:
      return '\u265e';
      break;
    case PieceEnum.ROOK:
      return '\u265c';
      break;
    case PieceEnum.QUEEN:
      return '\u265b';
      break;
    case PieceEnum.KING:
      return '\u265a';
      break;
    default:
      return '';
      break;
  }
}

function ChessBoard() {
  // enums and structs

  function Square(p, c) {
    this.piece = p;
    this.color = c;
  }

  //============================================================================
  // board is a 2D array of squares
  this.board = new Array(8);
  for (var i = 0; i < 8; i++) {
    this.board[i] = new Array(8);
  }

  // individually place pieces on home rows
  this.board[0] = [
    new Square(PieceEnum.ROOK, ColorEnum.WHITE),
    new Square(PieceEnum.KNIGHT, ColorEnum.WHITE),
    new Square(PieceEnum.BISHOP, ColorEnum.WHITE),
    new Square(PieceEnum.QUEEN, ColorEnum.WHITE),
    new Square(PieceEnum.KING, ColorEnum.WHITE),
    new Square(PieceEnum.BISHOP, ColorEnum.WHITE),
    new Square(PieceEnum.KNIGHT, ColorEnum.WHITE),
    new Square(PieceEnum.ROOK, ColorEnum.WHITE),
  ]
  this.board[7] = [
    new Square(PieceEnum.ROOK, ColorEnum.BLACK),
    new Square(PieceEnum.KNIGHT, ColorEnum.BLACK),
    new Square(PieceEnum.BISHOP, ColorEnum.BLACK),
    new Square(PieceEnum.QUEEN, ColorEnum.BLACK),
    new Square(PieceEnum.KING, ColorEnum.BLACK),
    new Square(PieceEnum.BISHOP, ColorEnum.BLACK),
    new Square(PieceEnum.KNIGHT, ColorEnum.BLACK),
    new Square(PieceEnum.ROOK, ColorEnum.BLACK),
  ]

  // place pawns
  for (var i = 0; i < 8; i++) {
    this.board[1][i] = new Square(PieceEnum.PAWN, ColorEnum.WHITE);
  }
  for (var i = 0; i < 8; i++) {
    this.board[6][i] = new Square(PieceEnum.PAWN, ColorEnum.BLACK);
  }

  // place empty squares
  for (var i = 2; i < 6; i++) {
    for (var j = 0; j < 8; j++) {
      this.board[i][j] = new Square(PieceEnum.EMPTY, ColorEnum.WHITE); // color is irrelevant for empty squares
    }
  }


  //============================================================================
  // returns an 8x8 array of bools indicating whether a piece can move to each given square
  this.GetValidMoves = function(x, y, king) {
    // initialise the array as all false
    var canMove = new Array(8);
    for (var i = 0; i < 8; i++) {
      canMove[i] = new Array(8);
      for (var j = 0; j < 8; j++) {
        canMove[i][j] = false;
      }
    }

    // check origin x,y is on board
    if (this.IsOnBoard(x,y)) {
      //--------------------------------------------------------------------------
      // helper method to check all squares in a straight line from the origin
      // dx and dy are the change in x and y for each step
      var thisBoard = this; //since 'this' refers to the window when used in nested functions
      var CheckLine = function(dx, dy) {
        // start one square away from the origin piece
        var checkx = x + dx;
        var checky = y + dy;

        // move one square at a time and add each empty square
        while (thisBoard.IsOnBoard(checkx,checky) && thisBoard.board[checky][checkx].piece == PieceEnum.EMPTY) {
          canMove[checky][checkx] = true;
          checkx += dx;
          checky += dy;
        }

        // when the line hits a piece, check if it can be taken
        if (thisBoard.IsOnBoard(checkx,checky)) {
          if (CanTake(checkx,checky)) {
              canMove[checky][checkx] = true;
          }
        }
      }

      // helper method to check if a piece can be taken
      var CanTake = function(tx, ty) {
        return (thisBoard.IsOnBoard(tx,ty) && thisBoard.board[ty][tx].color != color && thisBoard.board[ty][tx].piece != PieceEnum.EMPTY && ((thisBoard.board[ty][tx].piece != PieceEnum.KING) || king))
      }
      //--------------------------------------------------------------------------

      // get origin piece type
      var piece = this.board[y][x].piece;
      var color = this.board[y][x].color;

      switch (piece) {
        case PieceEnum.PAWN:
          if (color == ColorEnum.WHITE) {
            // white, move in positive y
            // move forward
            if (this.IsOnBoard(x,y+1) && this.board[y+1][x].piece == PieceEnum.EMPTY) {
              canMove[y+1][x] = true;
              // move 2 forward
              if (y == 1 && this.board[y+2][x].piece == PieceEnum.EMPTY) {
                canMove[y+2][x] = true;
              }
            }

            // diagonal captures
            if (CanTake(x-1,y+1)) {
              canMove[y+1][x-1] = true;
            }
            if (CanTake(x+1,y+1)) {
              canMove[y+1][x+1] = true;
            }
          }
          else {
            // black, move in negative y
            // move forward
            if (this.IsOnBoard(x,y-1) && this.board[y-1][x].piece == PieceEnum.EMPTY) {
              canMove[y-1][x] = true;
              // move 2 forward
              if (y == 6 && this.board[y-2][x].piece == PieceEnum.EMPTY) {
                canMove[y-2][x] = true;
              }
            }

            // diagonal captures
            if (CanTake(x-1,y-1)) {
              canMove[y-1][x-1] = true;
            }
            if (CanTake(x+1,y-1)) {
              canMove[y-1][x+1] = true;
            }
          }
          break;

        case PieceEnum.BISHOP:
          // check diagonals
          CheckLine(-1,-1);
          CheckLine(-1, 1);
          CheckLine( 1,-1);
          CheckLine( 1, 1);
          break;

        case PieceEnum.KNIGHT:
          // check x+-1,y+-2
          for (var i = -1; i <= 1; i += 2) {
            for (var j = -2; j <= 2; j += 4) {
              if (this.IsOnBoard(x+i,y+j) && (CanTake(x+i,y+j) || this.board[y+j][x+i].piece == PieceEnum.EMPTY)) {
                canMove[y+j][x+i] = true;
              }
            }
          }
          // check x+-2,y+-1
          for (var i = -2; i <= 2; i += 4) {
            for (var j = -1; j <= 1; j += 2) {
              if (this.IsOnBoard(x+i,y+j) && (CanTake(x+i,y+j) || this.board[y+j][x+i].piece == PieceEnum.EMPTY)) {
                canMove[y+j][x+i] = true;
              }
            }
          }
          break;


        case PieceEnum.ROOK:
          // check straights
          CheckLine( 0,-1);
          CheckLine( 0, 1);
          CheckLine(-1, 0);
          CheckLine( 1, 0);
          break;

        case PieceEnum.QUEEN:
          // check all straight lines
          CheckLine(-1,-1);
          CheckLine(-1, 1);
          CheckLine( 1,-1);
          CheckLine( 1, 1);
          CheckLine( 0,-1);
          CheckLine( 0, 1);
          CheckLine(-1, 0);
          CheckLine( 1, 0);
          break;

        case PieceEnum.KING:
          // check a 3x3 grid
          // the origin square will be excluded by the CanTake method
          for (var i = -1; i <= 1; i++) {
            for (var j = -1; j <= 1; j++) {
              if (this.IsOnBoard(x+i,y+j) && (CanTake(x+i,y+j) || this.board[y+j][x+i].piece == PieceEnum.EMPTY)) {
                canMove[y+j][x+i] = true;
              }
            }
          }
          break;

        default:
          // empty square or not initialised properly
          // should never happen but return all squares as false if it does
          break;
      }
    }

    return canMove;
  }

  // attempts to move a piece and returns whether it succeeded
  this.Move = function(ox, oy, tx, ty) { // origin and target x and y

    // if the target x,y is outside the board, fail
    if (this.IsOnBoard(ox,oy) && this.IsOnBoard(tx,ty)) {
      var originPiece = this.board[oy][ox].piece;
      var originColor = this.board[oy][ox].color;

      var targetPiece = this.board[ty][tx].piece;
      var targetColor = this.board[ty][tx].color;

      var validMoves = this.GetValidMoves(ox,oy,false);

      if (validMoves[ty][tx]) {
        // move is valid
        this.board[ty][tx] = new Square(originPiece, originColor);
        this.board[oy][ox] = new Square(PieceEnum.EMPTY, ColorEnum.WHITE);

        if (this.IsInCheck(originColor)) {
          // moving player is putting themself in check, undo the move and fail
          this.board[oy][ox] = new Square(originPiece, originColor);
          this.board[ty][tx] = new Square(targetPiece, targetColor);
          return false;
        }
        else {
          return true;
        }
      }
      else {
        // move is invalid
        return false;
      }
    }
    else {
      // target is outside the board
      return false;
    }
  }

  this.IsInCheck = function(color) {
    // find the king to check
    var kingX;
    var kingY;
    for (var checkx = 0; checkx < 8; checkx++) {
      for (var checky = 0; checky < 8; checky++) {
        if (this.board[checky][checkx].color == color && this.board[checky][checkx].piece == PieceEnum.KING) {
          kingX = checkx;
          kingY = checky;
        }
      }
    }

    var opponentColor = 3 - color; // inverts the colour
    var opponentPieces = this.GetColorSquares(opponentColor);

    for (var checkx = 0; checkx < 8; checkx++) {
      for (var checky = 0; checky < 8; checky++) {
        if (opponentPieces[checky,checkx]) {
          if (this.GetValidMoves(checkx,checky,true)[kingY][kingX]) {
            return true;
          }
        }
      }
    }

    return false;
  }

  this.IsInCheckmate = function(color) {
    return (this.IsInCheck(color) && !this.CanMove(color));
  }

  this.CanMove = function(color) {
    // for each piece of the player's colour
    var pieces = this.GetColorSquares(color);
    console.log(pieces);
    for (var oy = 0; oy < 8; oy++) {
      for (var ox = 0; ox < 8; ox++) {
        if (pieces[oy][ox]) {
          var originPiece = this.board[oy][ox].piece;
          var originColor = this.board[oy][ox].color;

          // for each square that the selected piece can move to
          var moves = this.GetValidMoves(ox,oy,false);
          for (var ty = 0; ty < 8; ty++) {
            for (var tx = 0; tx < 8; tx++) {
              if (moves[ty][tx]) {
                var targetPiece = this.board[ty][tx].piece;
                var targetColor = this.board[ty][tx].color;

                // try moving the piece
                if (this.Move(ox,oy,tx,ty)) {

                  // undo the move
                  this.board[oy][ox] = new Square(originPiece, originColor);
                  this.board[ty][tx] = new Square(targetPiece, targetColor);

                  // at least one valid move for the player exists
                  return true;
                }
              }
            }
          }
        }
      }
    }
    return false; // checked every possibility without finding a single valid move
  }

  // returns an 8x8 array of bools indicating whether a square conatins a coloured piece
  this.GetColorSquares = function(c) {
    var colorSquares = new Array(8);
    for (var i = 0; i < 8; i++) {
      colorSquares[i] = new Array(8);
      for (var j = 0; j < 8; j++) {
        colorSquares[i][j] = (this.board[i][j].color == c && this.board[i][j].piece != PieceEnum.EMPTY);
      }
    }
    return colorSquares;
  }

  // helper method to check if x,y coordinates are valid
  this.IsOnBoard = function(x, y) {
    return (x >= 0 && x < 8 && y >= 0 && y < 8)
  }
}
